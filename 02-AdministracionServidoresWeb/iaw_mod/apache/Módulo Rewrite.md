﻿


> Written with [StackEdit](https://stackedit.io/).
> # Módulo Rewrite
> 
> El módulo  [`rewrite`](http://httpd.apache.org/docs/current/mod/mod_rewrite.html)  nos va a permitir usar una URL e internamente estar accediendo a otra URL. Ayudado por los ficheros  `.htaccess`, el módulo  `rewrite`  nos va a ayudar a formar **URL amigables** que son mejor tratadas por los motores de búsquedas y mejor recordadas por los humanos. Por ejemplo estas URL:

```
www.dominio.com/articulos/muestra.php?id=23
www.dominio.com/pueblos/pueblo.php?nombre=melide

```

Es mucho mejor escribirlas como:

```
www.dominio.com/articulos/23.php
www.dominio.com/pueblos/melide.php

```
El módulo rewrite se utiliza por gestores de contenidos como Wordpress para poder seleccionar formatos más amigables para construir las URL de las páginas y artículos de nuestro sitio web.


## Ejemplo 1: Cambiar la extensión de los ficheros

Si subes al document root de apache el fichero operaciones.php en   https://bitbucket.org/bardals/wiki-iaw/src/master/02-AdministracionServidoresWeb/ejercicios/mod_rewrite/ podrías usarlo de la siguiente manera:

```
    http://www.pagina1.org/operacion.php?op=suma&op1=6&op2=8

```

Y si queremos reescribir la URL y que usemos en vez de php html, de esta forma:

```
    http://www.pagina1.org/operacion.html?op=suma&op1=6&op2=8

```

Para ello activamos el  `mod_rewrite` con `a2enmod rewrite`, y escribimos p.ej. en el document root del virtual host que queramos usar, el fichero `.htaccess`:

```
    Options FollowSymLinks
    RewriteEngine On
    RewriteBase /
    RewriteRule ^(.+).html$ $1.php [nc]
```
Este código lo podríamos escribir también en el fichero de configuración del virtualhost
El flag  `[nc]`  lo ponemos para no distinguir entre mayúsculas y minúsculas.

Ahora mismo se puede acceder a través de la URL con *operacion.php** o a través de la URL con *operacion.html*, i.e, la misma página con dos URL distintas. Esto puede ser penalizado por los motores de búsqueda. Para solucionar esto podemos añadir una redirección:

```
    RewriteRule ^(.+).html$ $1.php [r,nc]

```
Ahora al escribir en la url *operacion.php* cambia en la url a *operacion.html*

## Ejemplo 2: Crear URL amigables

Creando una URL amigable podríamos llamar a este fichero de la siguiente manera:

```
    http://www.pagina1.org/suma/8/6

```

Escribimos un  `.htaccess`  de la siguiente manera:

```
    Options FollowSymLinks
    RewriteEngine On
    RewriteBase /
    RewriteRule ^([a-z]+)/([0-9]+)/([0-9]+)$ operacion.php?op=$1&op1=$2&op2=$3

```
