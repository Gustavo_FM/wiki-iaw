﻿


## FTP seguro

Tradicionalmente, o servizo FTP sempre foi inseguro. Cando un usuario se conecta, o seu nome de usuario e contrasinal, son transmitidos sen encriptar, podendo pasar, que alguen intercepte a comunicación e se faga coas credenciais do usuario. Afortunadamante, pódese empregar facendo que o servidor FTP empregue autenticación de OpenSSL, facendo que o nome de usuario, o seu contrasinal e tamén os datos transferidos vaian encriptados.

Para conseguir isto, o primeiro paso é crear o certificado SSL,

     sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/vsftpd.pem -out /etc/ssl/certs/vsftpd.pem

A continuación editamos o ficheiro de configuración de vsftp e introducimos as seguintes directivas (Se algunha delas está configurada, eliminámola):

    ssl_enable=YES
    allow_anon_ssl=NO
    force_local_data_ssl=NO
    force_local_logins_ssl=NO
    ssl_tlsv1=YES
    ssl_sslv2=NO
    ssl_sslv3=NO
    rsa_cert_file=/etc/ssl/certs/vsftpd.pem
    rsa_private_key_file=/etc/ssl/private/vsftpd.pem

Se queremos forzar a que os usuarios locais so se poidan conectar empregando SSL, cambiamos as seguintes directivas.

    force_local_data_ssl=YES
    force_local_logins_ssl=YES

No caso de erro na conexión SSL desde Filezilla, engadir o seguinte parámetro:

    ssl_ciphers=AES128-SHA

