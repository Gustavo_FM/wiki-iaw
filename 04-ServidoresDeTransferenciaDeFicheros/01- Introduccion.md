﻿


> Written with [StackEdit](https://stackedit.io/).
 

## Introdución aos servidores de transferencia de ficheiros (FTP)

FTP (File Transfer Protocol, Protocolo de Transferencia de Arquivos), é un protocolo de rede para a transferencia de arquivos entre sistemas conectados a unha rede TCP, baseado na arquitectura cliente-servidor.

Dende un equipo cliente pódese conectar a un servidor para descargar arquivos dende el ou ben para enviarlle arquivos, independentemente do sistema operativo utilizado en cada equipo.

O servizo permite aos clientes:

 - Acceder a sistemas para listar directorios e arquivos.
 - Transferir arquivos dende ou cara ao sistema remoto.
 - Operacións como borrar arquivos, crear directorios, etc.

O protocolo FTP é así o conxunto de normas e regras que permiten o dialogo entre máquinas .

O servizo FTP é ofrecido pola capa de aplicación do modelo de capas de rede TCP/IP e utiliza TCP como protocolo de transporte. Normalmente emprega os portos de rede 20 e o 21. O 20 utilízase para o fluxo de datos entre o cliente e o servidor e o porto 21 para o fluxo de control, é dicir, para enviar as ordes do cliente ao servidor.

Mentres se transfiren datos a través do canal de datos, o fluxo de control permanece á espera.

Diferenciamos dous roles principais no protocolo FTP:

**Clientes FTP.** Acceden ao sistema de arquivos do equipo onde están instalados e establecen conexións cos servidores FTP para enviar o descargar arquivos.

**Servidores FTP.** Acceden ao sistema de arquivos do equipo onde están instalados, manexan as conexións dos clientes e en función dos permisos dos usuarios permiten a descargar ou subida de arquivos.

As máquinas servidor e clientes manteñen conexións TCP independentes para control e para transferencia de datos.

**Conexión de control.** O cliente establece unha conexión co servidor que lle permite dialogar con el. Envíalle os comandos e recibe as respostas do servidor que lle informa de se houbo erros ou se vai todo ben. Esta conexión está activa todo o tempo ata que o cliente pecha a sesión ou ata que pasa un tempo sen actividade, entón o servidor pecha a sesión (timeout).

**Conexión de datos.** Cando o cliente solicita unha transferencia, créase unha nova conexión de datos que se pecha ao final da transmisión. Poden existir varias conexións de datos ligadas a unha única conexión de control.

**Tipos de acceso**

Os servidores permiten, dependendo da súa configuración, dous tipos de acceso dende os clientes:

**Acceso anónimo**. O cliente se conecta cun usuario especial chamado anonymous. Este usuario non ten contrasinal e, habitualmente, só pode acceder a un directorio para descargar ficheiros.

**Acceso autorizado**. O cliente FTP conéctase cun nome de usuario que está validado no sistema. Pode ser que estea validado localmente no sistema ou unicamente como usuario do servizo FTP. O servidor ten establecido permisos para os usuarios autenticados de modo que algúns poderán subir e descargar arquivos e outros unicamente descargar.

**Modos e tipos de transferencia**

Como xa se explicou antes, en toda transferencia FTP intervén un programa servidor e un programa cliente. O programa servidor execútase na máquina onde están almacenados os arquivos que se ofrecen para descargar/subir e o programa cliente é o programa FTP que usamos dende a máquina local para subir ou descargar os arquivos.

**Modo activo**
![enter image description here](img/modo_activo.png)
O cliente conéctase dende un porto aleatorio n (n > 1024) ao porto de control do servidor (porto 21).

O cliente comeza a escoitar no porto n+1 e envía este porto de control ao servidor.

O servidor conéctase ao cliente polo porto n+1 dende o porto de datos 20.

Chámase modo activo porque é o servidor o que inicia a transmisión de datos, cara ao porto que se lle indica.

**Modo pasivo**

Cando o cliente está detrás de unha devasa pode darse o caso que o servidor non poida iniciar a conexión de datos, é por isto que se emprega o modo pasivo.

O cliente abre dous portos n (n > 1024) e n+1

O primeiro porto, utilízase como porto de control e conéctase ao porto 21 do servidor para lle enviar o comando PASV.

O servidor abre un porto p (p>1024) e envía o comando PORT p ao cliente.

O cliente inicia a conexión dende o porto n+1 ao porto p do servidor para transferir datos.
![enter image description here](img/modo_pasivo.png)

En modo pasivo é o cliente o que inicia sempre as conexións co servidor. O porto 20 do servidor non se utiliza.

**Tipos de transferencia de arquivos**

En FTP hai dous modos de transferencia de arquivos: ASCII e binario.

**ASCII (type ascii).** Transmítese byte a byte. Utilízase para arquivos de texto.

**Binario (type bin**). Transmítese bit a bit. Para arquivos que no son de texto: executables, imaxes, etc.
