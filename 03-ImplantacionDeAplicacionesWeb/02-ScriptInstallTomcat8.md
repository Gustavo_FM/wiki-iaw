# Pasos para instalar Tomcat 8 en Ubuntu 16.04

## Primer paso: Instalar JDK

```shell
# Actualizar referencias
apt-get update

# Instalar JDK y lo que me haga falta (curl, vim, tree...)
apt-get install -y default-jdk curl vim tree
```

## Segundo paso: Instalar con APT Tomcat8

```shell
apt-get install -y tomcat8
```

## Tercer paso: Arrancar y Acceder a la página `default` de Apache Tomcat

```shell

# Comprobamos si el servicio está activo:
service tomcat8 status

# En caso de que no esté activo, arrancamos tomcat
service tomcat8 start

# Se accederá a través de un agente Web (navegador) puerto por defecto 8080 o compramos con curl
curl http://localhost:8080
```

## Accedemos a los resursos de documentación de Tomcat (sólo en ambientes de desarrollo)

```shell
# Instalamos la documentación en la máquina local
apt-get install -y tomcat8-docs

# Instalamos los ejemplos en la máquina local de desarrollo en tomcat
apt-get install -y tomcat8-examples

# Instalación de 2 Web Apps para Gestionar Tomcat
apt-get install -y tomcat8-admin
```

## Accedamos a la Web App Manager

La primera vez que tratamos de acceder a la aplicación nos pide unas credenciales. No las tenemos. Y por tanto, al darle a `Cancelar` o `Aceptar`, nos va a devolver un código de estado HTTP `401` con una página que nos da instrucciones al respecto de cómo actuar para tener accedo.

Modificamos el fichero: `/var/lib/tomcat8/conf/tomcat-users.xml` y añadimos estas líneas:

```xml
 <role rolename="manager-gui"/>
 <user username="tomcat" password="tomcat" roles="manager-gui"/>
```


## Referencias externas

* [how-to-install-apache-tomcat-8-on-ubuntu-16-04](https://www.digitalocean.com/community/tutorials/how-to-install-apache-tomcat-8-on-ubuntu-16-04) 