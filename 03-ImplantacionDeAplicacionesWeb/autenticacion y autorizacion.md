﻿


> Written with [StackEdit](https://stackedit.io/).
### Autenticación y Autorización

Tmcat permite proteger el acceso a las aplicaciones que tiene desplegads mediante distintos tipos de autenticación. En este [enlace](https://tomcat.apache.org/tomcat-9.0-doc/realm-howto.html) tienes toda la información al respect en el sitio oficial de Tomcat.
En este documento vamos a ver cómo configurar el acceso a una aplicación sample con autenticación BASIC para que sólo puedan acceder dos usuarios profesor y alumno del curso de DAW ( role DAW). La información sobre usuarios y roles se almacenarán en el fichero _$CATALINA_BASE/conf/tomcat-user.xml_ 

1. Nos aseguramos que la aplicación sample está desplegada.

2. Añade al fichero *$CATALINA_BASE/conf/tomcat-user.xml*  los usuarios alumno y profesor con el role curso.

3. UserDatabaseRealm es el mecanismo Realm que se usa a nivel de host o sitio virtual(ver server.xml). Se pueden definir a nivel de Engine,Host,Aplicación.
Si se quiere usar otro Realm se puede definir en el contexto de la aplicación, creando en ese caso un fichero context.xml dentro de la carpeta META-INF donde está desplegada la aplicación sample con el siguiente contenido:

    
    <Context>  
        <Realm classname="org.apache.catalina.realm.MemoryRealm" />  
        </Context>



5. Editamos el descriptor de despliegue ( web.xml que está dentro de WEB-INF ) y añadimos los siguientes elementos:


    <security-constraint>  
    <web-resource-collection>  
    <web-resource-name>recurso protected</web-resource-name>  
    <url-pattern>/url_recurso_protected/*</url-pattern>  
    </web-resource-collection>  
    <auth-constraint>  
    <role-name>role1</role-name>
    <role-name>role2</role-name>	
    </auth-constraint>  
    </security-constraint>  
    <login-config>  
    <auth-method>BASIC</auth-method>  
    <realm-name>Acceso a la aplicación</realm-name>  
    </login-config>  
	
	

7. Reinicia Tomcat y arranca la aplicación desde el Web Manager.
8. Accede a http://ip_server:8080/directorio_aplicacion/url_recurso_protected
9. Para más info revisa la configuración de la aplicacion de ejemplo de Tomcat para autenticación del acceso al recurso de la aplicación http://localhost:8080/examples/jsp/security/protected/
