﻿


> Written with [StackEdit](https://stackedit.io/).

>## Aplicaciones web en python con módulo wsgi de apache

Veamos un ejemplo de configuración para una aplicación django. 
Django es un framework de desarrollo de aplicaciones python. Vamos a instalarlo para ejecutarlo con python3:

    apt install python3-django

A continuación desde el directorio */var/www/mipython* creamos un proyecto en el framework con su estructura de directorios y ficheros  con

    django-admin startproject myapp

Crea en  */var/www/mipython* la estructura de directorios y ficheros del proyecto.
Las peticiones a cualquier recurso python son tratadas mediante el protocolo wsgi, por lo que el punto de entrada para ejecutar cualquier aplicación web python del proyecto va a ser:  `/var/www/mipython/myapp/myapp/wsgi.py` 

Este fichero recibe la petición, la trata y genera la respuesta para el servidor web.
Además modificamos el fichero `/var/www/mipython/myapp/myapp/settings.py`
para permitir el acceso desde cualquier host. 
```
nano myapp/myapp/settings.py
	ALLOWED_HOSTS = ["*"]
```
Creamos tb una base de datos SQLite para nuestra aplicación con:

    python3 manage.py migrate

y un pequeño servidor web par ir probando la aplicación en un entorno de desarrollo con:

    python3 manage.py runserver 0.0.0.0:8000
y en http://server_name  server_ip:8000 tenemos la página de inicio de la aplicación.

Para que apache pueda ejecutar aplicaciones web en python instalamos el módulo   `libapache2-mod-wsgi`  si vamos a trabajar con python2 o  `libapache2-mod-wsgi-py3`  con python3 (como este caso).
Comprobamos que el módulo se activa al instalarlo.
Creamos un sitio virtual en apache con la siguiente configuración:
```
<VirtualHost *>
    ServerName mipython.com
    DocumentRoot /var/www/mipython/myapp
    WSGIDaemonProcess myapp user=www-data group=www-data processes=1 threads=5 python-path=/var/www/mipython/myapp
    WSGIScriptAlias / /var/www/html/myapp/myapp/wsgi.py

    <Directory /var/www/mipython/myapp>
            WSGIProcessGroup myapp
            WSGIApplicationGroup %{GLOBAL}
            Require all granted
    </Directory>
</VirtualHost>

```
Activamos el virtual host y reiniciamos el servidor apache. De esta forma, con http://mipython.com tenemos la página de inicio de la aplicación. (ya responde el propio servidor apache)
>## Aplicaciones web en python con servidores wsgi

Otra forma de ejecutar código python es usar **servidores de aplicación wsgi.** En este escenario usamos apache2 como proxy inverso que envía la petición python al servidor WSGI que estemos utilizando, frente al escenario anterior en el que habilitamos un módulo de apache para que éste ejecutara las peticiones a aplicaciones web en python. Con ellos conseguimos mejorar el rendimiento en la ejecución de aplicaciones web en python.

Tenemos a nuestra disposición varios servidores:  [A Comparison of Web Servers for Python Based Web Applications](https://www.digitalocean.com/community/tutorials/a-comparison-of-web-servers-for-python-based-web-applications). 
En este ejemplo vamos a hacer uso del **servidor uwsgi:**
### uwsgi (modo desarrollo)

Para instalarlo en Debian 9 Stretch e instalar el plugin para que trabaje con python3 (uwsgi puede trabajar con varios lenguajes de programación).
```
apt install uwsgi
apt install uwsgi-plugin-python3

```
También lo podemos instalar con  `pip`  en un entorno virtual.

**Despliegue de una aplicación django con uwsgi**

Podemos abrir la aplicación django anterior con el nuevo servidor **uwsgi** ejecutando:
```
uwsgi --http :8080 --plugin python --chdir /var/www/mipython/myapp/myapp --wsgi-file myappapp/wsgi.py --process 4 --threads 2 --master 

```
Otra alternativa es crear un fichero  `ejemplo.ini`  con la  configuración de los parámetros anteriores
```
[uwsgi]
http-socket = :8080
chdir = /var/www/mipython/myapp/myapp 
wsgi-file = myapp/wsgi.py
processes = 4
threads = 2
plugin = python3

```
Y ejecutamos el servidor pasándole el path del fichero de configuración:
```
uwsgi ejemplo.ini

```

De esta forma puedo tener varios ficheros de configuración del servidor uwsgi para las distintas aplicaciones python que sirva el servidor.

Podemos tener los ficheros de configuración en  `/etc/uwsgi/apps-available`  y para habilitar podemos crear un enlace simbólico a estos ficheros en  `/etc/uwsgi/apps-enabled`.

En el ejemplo anterior hemos usado la opción  `http`  para indicar que se va a devolver una respuesta HTTP, podemos usar varias opciones:

-   `http`: Se comporta como un servidor http.
-   `http-socket`: Si vamos a utilizar un proxy inverso usando el servidor uwsgi.
-   `socket`: La respuesta ofrecida por el servidor no es HTTP, es usando el protocolo uwsgi.

Existen muchas más opciones que puedes usar:  [](http://uwsgi-docs.readthedocs.io/en/latest/Options.html)[http://uwsgi-docs.readthedocs.io/en/latest/Options.html](http://uwsgi-docs.readthedocs.io/en/latest/Options.html).

## Apache con uwsgi (modo producción)

Usaremos apache2 como proxy inverso que envía la petición python al servidor WSGI que estemos utilizando.

### Configuración usando HTTP
Con esta configuración, el servidor uwsgi responde al servidor apache usando protocolo http:
Desactivamos el módulo wsgi de apache que activamos en el aptdo : `a2dismod wsgi`
Necesitamos instalar el módulo de proxy de http:
```
# a2enmod proxy proxy_http

```
La configuración del virtual host podría quedar:
```
<VirtualHost *:80> 
    ServerName mipython.com
    ServerAdmin webmaster@localhost
    ProxyPass / http://localhost:8080/
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

```
(acordarse de eliminar la configuración para el módulo wsgi de apache, si la habíamos hecho antes)
Como vemos el contenido estático es servido por Apache y el procesamiento de python se pasa al servidor uwsgi.

### Configuración usando uWSGI
Con esta configuración, el servidor uwsgi responde al servidor apache usando protocolo wsgi, aumentando su rendimiento. 

En este caso en el fichero de configuración de uwsgi,  `ejemplo.ini`, tendríamos que sustituir la configuración
*http-socket = :8080* por:
```
...
socket = 127.0.0.1:3032
...

```
(3032 es un puerto cualquiera)
y deshabilitar el módulo proxy_http `a2dismod proxy_http`
Para la comunicación con uwsgi, apache necesita un módulo llamado  `proxy_uwsgi`:

```
# apt install libapache2-mod-proxy-uwsgi
# a2enmod proxy_uwsgi

```
Y modificamos el fichero de configuración para el servidor virtual que servirá a aplicación django.
```
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    ProxyPass / uwsgi://127.0.0.1:3032/
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```
Se reinicia el servidor apache, se arranca el servior wsgi (pq no está configurado como servicio) y en http://servername el servidor apache nos ofrece la respuesta a la petición enviada por el servidor uwsgi mediante protocolo wsgi.
