![Imagen Cabedera Página](img/405-Method-Not-Allowed-t.jpg)

# El error 405

Los servidores web informan a los clientes (los navegadores, por ejemplo) sobre el estado de las consultas realizadas con ayuda de **códigos de estado HTTP**. 

Existen diferentes códigos para confirmar el éxito o el fracaso de las solicitudes y, entre ellos, también mensajes muy específicos. Mientras que algunos de estos avisos tienen lugar cuando se navega diariamente por la World Wide Web,el `error 405 Method Not Allowed` se cuenta entre los fallos que rara vez se notifican. 

A continuación te contamos qué es lo que genera estos mensajes de error y por qué la solución al problema recae en los administradores de las páginas web.

## ¿Qué se esconde tras el error 405 HTTP?

El Hypertext Transfer Protocol (HTTP) define métodos que designan posibles acciones que se ejecutan en el servidor web contactado. Entre ellos se engloban:

* GET: extrae los datos vinculados a un recurso de URL determinado
* HEAD: extrae los datos del encabezado vinculados a un recurso de URL
* POST: envía datos al servidor web, como datos de formularios
* PUT: sustituye los datos para un URL determinado por los nuevos que ha facilitado el cliente
DELETE: elimina los datos que hay detrás del URL correspondiente


El administrador puede configurar cada servidor web de modo que sea capaz de permitir o denegar cada uno de los métodos. Si la página web carece de contenido interactivo es lógico que no se autorice el método POST, ya que el usuario no tiene la posibilidad ni de introducir sus propios datos ni de enviarlos al servidor web. Si, por el contrario, la página incluye un formulario, el servidor tiene que autorizarlo. De no ser así, se recibirá el mensaje de error status code 405, que informa al navegador y a sus usuarios que el método aplicado no está autorizado, en inglés Method not Allowed.

El texto literal de un mensaje `405 HTTP` puede variar en función del servidor, pero algunas de las fórmulas más conocidas son las siguientes:

```
* 405 Method Not Allowed
* 405 Not Allowed
* Method Not Allowed
* HTTP 405 Error
* HTTP Error 405 – Method Not Allowed
* HTTP 405 Method Not Allowed
* Error: 405 Method Not Allowed
* 405 – HTTP verb used to access this page is not allowed
* HTTP Status 405 – HTTP method GET is not supported by this URL
```

## ¿En qué situaciones se produce el error 405?

Al comienzo de nuestro artículo ya se ha indicado que el 405 error es un fallo que puede atribuirse a un problema del lado del servidor. No obstante, esto no parece tener sentido pues el status `code 405` es concebido desde un punto de vista técnico como uno de los mensajes de **error del cliente** (códigos con el patrón `4xx`).

Sin embargo, esta contradicción se entiende fácilmente de la siguiente manera: si, como usuario de un navegador, envías una petición al servidor web con un método HTTP que este no autoriza debido a su configuración, **desde el punto de vista del servidor el error reside en el lado del cliente**, que habría realizado, según él, una pregunta incorrecta. El servidor, al procesar esta petición, no sabe que solo quieres acceder a la página web para rellenar un formulario.

En concreto, tres son las situaciones que pueden dar lugar al `HTTP 405 code`:  

1. La prohibición del método HTTP correspondiente tiene su origen en una **configuración errónea** del servidor web o de los componentes de software encargados de ejecutar la acción correspondiente para el **recurso de URL** deseado.

2. El webmaster ha previsto la prohibición del método HTTP, en la mayoría de los casos por motivos de seguridad. El error reside en un recurso de URL del proyecto web en cuestión que, a causa de su programación,solicita el método no autorizado.

3. **El proveedor de hosting** del administrador de la página web no autoriza el método HTTP, lo que puede ocurrir con el **método POST**, necesario para la introducción de datos y, que en el caso de algunos proveedores, está bloqueado a la hora de acceder a documentos HTML por motivos de seguridad.

## Forma de reproducirlo

```bash
# Levantamos un servidor web
docker run -d --name webServer -p 80:80 nginx

# Realizamos una petición post
curl --verbose -d hola http://localhost

# Ver logs del servidor
docker logs webServer
```

## Fuente

* [Amplía más leyendo el artículo completo](https://www.ionos.es/digitalguide/hosting/cuestiones-tecnicas/el-error-405-que-es-y-como-solucionarlo/)

## Recursos adicionales

* [Protocolo de transferencia de hipertexto en la wikipedia](https://es.wikipedia.org/wiki/Protocolo_de_transferencia_de_hipertexto)

* [RFC 2616 - Hypertext Transfer Protocol -- HTTP/1.1](https://tools.ietf.org/html/rfc2616)