﻿


> Written with [StackEdit](https://stackedit.io/).
> 

# Páginas web estáticas

Es el primer tipo de páginas web que se crearon y del que todavía podemos encontrar muchos casos en Internet.
Se basa en el  **lenguaje  _[HTML](http://es.wikipedia.org/wiki/HTML)_**, el cual es interpretado por los navegadores web para mostrar la información al usuario.
Las páginas web creadas  **exclusivamente con lenguaje  _HTML_  no variarán**  en su forma ni en su contenido mientras su desarrollador no modifique el código de la página. 

El lenguaje de marcas  [CSS](http://es.wikipedia.org/wiki/Css)  se puede utilizar junto con el lenguaje  _HTML_  para facilitar el diseño del aspecto visual que ofrecen las páginas web, aunque su uso por sí solo no permitirá crear dinamismo en las páginas web.
# Aplicaciones web

Algunos lenguajes de programación permiten integrar código junto con el lenguaje  _HTML_  para ofrecer  **contenidos dinámicos**  en las páginas web. De esta manera la web crece enormemente en posibilidades y da origen a las  [**aplicaciones web**](http://es.wikipedia.org/wiki/Aplicaci%C3%B3n_web).

## Aplicaciones web dinámicas del lado cliente

### Lenguajes embebidos

Los casos más sencillos de desarrollo de aplicaciones web son los lenguajes de programación que son  **interpretados directamente por los navegadores**  (lenguajes de programación web del lado cliente) . El navegador, al recibir el contenido de la página web con código escrito en ese tipo de lenguajes , será el responsable de interpretarlo para ofrecer el resultado de su ejecución al usuario.

Uno de los casos más utilizados de este tipo de lenguajes es [_**JavaScript**_](http://es.wikipedia.org/wiki/JavaScript). Este lenguaje se puede integrar dentro del código _HTML_ debiendo con la etiqueta `<script>`


[Aquí](https://bardals.bitbucket.io/reloj.html) puedes ver un ejemplo de una página web que muestra un reloj que se va actualizando cada segundo.


## Aplicaciones web dinámicas del lado servidor

 El  **servidor web puede devolvernos una información u otra**  en una misma página web en función de la interacción del usuario o de cualquier otra circunstancia.
 La página web enviada deberá emplear los lenguajes reconocidos por el navegador (HTML, CSS, JavaScript, etc), ya que los **navegadores web no reconocen los lenguajes**  de programación que se emplean en los  **servidores web**. 
 En este tipo de aplicaciones es muy frecuente que el servidor web acceda a una  **base de datos**  para obtener los datos que finalmente formarán parte de la página web.

Algunas técnicas y lenguajes de programación para el desarrollo de aplicaciones web del lado servidor que **generan el código web para el cliente** son:

### CGI

El  _[Common Gateway Interface](http://es.wikipedia.org/wiki/Interfaz_de_entrada_com%C3%BAn)  (CGI)_  es un método estándar empleado por servidores web para delegar la generación del contenido web a archivos ejecutables. 
Las [aplicaciones CGI](https://www.ionos.es/digitalguide/paginas-web/desarrollo-web/common-gateway-interface/) fueron unas de las primeras técnicas empleadas para crear contenido dinámico para las páginas web, aunque su uso sigue muy extendido
Es muy habitual el uso de [**Perl**](http://es.wikipedia.org/wiki/Perl)  o [**Python**](https://docs.python.org/2/library/cgi.html) para el desarrollo de scripts CGI por su facilidad en el manejo de cadenas de caracteres. aunque su uso sigue muy extendido en la actualidad. 
Con CGI, no es necesario que todo el contenido de la página HTML esté disponible en el servidor, sino que este se genera de forma dinámica cuando el usuario realiza la solicitud correspondiente a través de la propia página.

[Aquí](https://bitbucket.org/bardals/bardals.bitbucket.io/src/master/reloj.cgi) puedes ver un ejemplo de código fuente Perl que crea un script CGI para mostrar la fecha actual en una página web

### Servlets

Un  [Servlet](http://es.wikipedia.org/wiki/Servlet)  es una tecnología web basada en Java para el lado servidor. Es una clase Java de  **Java EE**  que utiliza la API Java Servlet, y su código suele tener  **código HTML embebido**.

El  **contenido generado**  por el Servlet suele ser HTML pero puede generar otros tipos de datos como XML.

Para ejecutar un Servlet es necesario usar un  **contenedor**  web (también conocido como contenedor de Servlets) que es un componente de un servidor web que interactúa con los Servlets.  Ej: *Tomcat, JBoss,,,etc*. El contenedor es responsable de manejar el ciclo de vida de los Servlets, mapear una URL a un Servlet en particular y asegurarse de que el solicitante de la URL tiene los permisos de acceso adecuados. 

Las  **ventajas**  de usar Servlets en vez de CGI es que los primeros tienen mejor rendimiento y facilidad de uso combinado con más potencia.

[Aquí](https://bitbucket.org/bardals/bardals.bitbucket.io/src/master/Form.java) puedes ver un  **ejemplo**  de código fuente de un Servlet que obtiene los datos introducidos por el usuario en un formulario anterior

### Lenguajes embebidos en el código HTML (PHP, JSP)

A diferencia de los lenguajes anteriores, cuyo código genera etiquetas HTML, existen otros lenguajes de programación de aplicaciones web del lado servidor cuyas  **instrucciones se encuentran embebidas en el código HTML**. Es el caso de lenguajes como  **PHP o JSP**.

El servidor tiene almacenadas los archivos que contienen el código correspondiente a las páginas web. Dichos archivos contienen etiquetas HTML y código en otro lenguaje que es  **interpretado por el servidor**, ejecutando sus instrucciones con las que se  **obtiene un código reconocible por el navegador**  cliente (HTML, CSS, JavaScript, etc). El resultado final obtenido es el que se envía al cliente.

Ejemplos de lenguajes de esta categoría:

-   [PHP](http://es.wikipedia.org/wiki/Php):
    -   El código es interpretado por un servidor web con un módulo procesador de PHP que genera la página web resultante. 
    - PHP se puede emplear en la mayoría de los servidores web de los principales sistemas operativos de forma gratuita al ser software libre. Por ejemplo se usa en gestores de contenido web como  _Joomla_  o  _Drupal_, blogs como  _Wordpress_, o aulas virtuales como  _Moodle_.
[Ejemplo](https://bardals.bitbucket.io/ConexionBD.php)

-  [JSP](http://es.wikipedia.org/wiki/JSP): JavaServer Pages es otra tecnología para crear dinámicamente páginas web similar a PHP pero usando el lenguaje de programación  **Java**. Para utilizarla es necesario disponer de un  **contenedor de servlets**  como  [Apache Tomcat](http://es.wikipedia.org/wiki/Tomcat).
JSP requiere que se tenga un compilador Java en el servidor web. El servidor web detecta el código de Java en el código HTML y luego lo envía al compilador de Java para su procesamiento. Cualquier salida de Java el programa la envía al navegador del cliente como parte del documento HTML.

Ver ejemplo [mvc-servlet-jsp](https://www.baeldung.com/mvc-servlet-jsp)
