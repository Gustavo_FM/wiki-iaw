﻿
### Programar en WordPress sólo con CSS

Quizás la manera más simple de empezar a programar es en la parte  **estética**, personalizando un tema con  **CSS**.

Esto es, además, un excelente ejemplo de cómo con saber programar tan solo un poquito, algo que hayas aprendido en cuestión de horas, rompes barreras que parecían infranqueables.

Concretemos el ejemplo:

Imagínate el típico escenario de que has dado con un tema WP  del repositorio gratuito de WordPress que te gusta, pero tiene el típico defecto de estos temas: no puedes personalizar casi nada en la configuración del tema y es que no te gusta nada ni el tipo de letra del texto, ni su tamaño y no hay ninguna opción por ningún lado para meterle mano a esto.

Pues bien, si aprendes CSS, descubrirás que solucionar este problema estético (y prácticamente cualquier otro)  **realmente es facilísimo**  porque puedes crear  **una hoja de estilos que modifique estas propiedades**.

Además, en la actualidad, WordPress, ya incorpora de manera estándar (menú  _«Apariencia/Editar CSS»_) la posibilidad de  **personalizar**  el CSS del tema que tengas activo con reglas CSS adicionales que te permiten modificar las reglas CSS por defecto.

Sólo tienes que averiguar cuáles son las reglas originales que usa ese tema y modificar las propiedades que te interesen en tus reglas sustitutas. Averiguarlas es bastante fácil usando las herramientas de las que disponen los navegadores habituales

### Programar en WordPress con PHP

Estando hecho WordPress en PHP y pudiendo acceder a sin restricciones a su código (que son los ficheros .php que verás en tu hosting), es muy típico que la gente empiece a hacer sus primeros pinitos en PHP.

El problema de esto es que suelen toquetear de una manera bastante  **imprudente**  el código de WordPress, sin ningún conocimiento de cómo se programa realmente en WordPress.

Esto es una  **muy mala práctica**  que te la quiero  **desaconsejar**  totalmente porque, aparte de poder provocar la famosa  [pantalla blanca de la muerte de WordPress](https://ayudawp.com/la-pantalla-blanca-de-la-muerte/), puedes provocar problemas mucho menos obvios a primera vista que te pueden pasar una factura muy cara al cabo de un tiempo.

Uno de los ejemplos más claros es lo que la gente hace en los temas WordPress. Si en vez de hacer las modificaciones estéticas por una vía correcta como la arriba descrita con reglas CSS añadidas con un plugin al tema las implementases modificando directamente el código PHP del tema, como hace mucha gente, ¿qué pasaría?

Pues al principio nada, que es lo malo, verías los cambios y aparentemente todo estaría bien.

Imagínate que, tras la buena experiencia, sigues cambiando y cambiando, pasa un año y te has quedado realmente contento con el resultado de tu trabajo, tras invertir un montón de horas para conseguirlo.

Un buen día, ves que hay una actualización del tema y, como no, lo actualizas, es recomendable.

Te vas otra vez al blog y, ¡horror! ¡Han  **desaparecido**  todas tus modificaciones!

_¿Por qué?_

Porque cuando WordPress actualiza un tema o plugin,  **borra por completo la versión anterior**  del tema o el plugin y con ello también  **todas tus modificaciones**.

Te la has cargado de un plumazo porque no sabías que un tema no se debe modificar así, a pelo, sino a través de lo que se conoce como un  **tema hijo**  (luego veremos lo que es esto).

Esto es un buen ejemplo de lo que puede pasar si no te tomas las molestias de aprender cómo se ha de programar en WordPress.

## Programar con el API de WordPress

Programar bien en WordPress quiere decir, respetar y usar su API.

Un API (Interfaz de Aplicación, Application Programming Interface en inglés) lo podríamos definir como un conjunto de funciones o servicios que un determinado producto ofrece para programar contra él pudiendo reutiliazar así para tus fines las funcionalidades que ese producto ya ha implementado, junto con una serie de reglas de lo que se debe y no debe hacer.

WordPress tiene un API enorme con una cantidad de funcionalidad enorme. Si queremos programar bien en WordPress, debemos usar el API siempre que sea posible. Ya no por aprovechar el trabajo hecho, sino por no “romper” el funcionamiento correcto de WordPress como pasaba en el ejemplo anterior de modificar “a pelo” un tema WordPress sin pasar por un tema hijo.

Por suerte, el API de WordPress está muy bien organizado y se divide, en realidad, en muchos APIs diferentes, según lo que se quiera hacer concretamente. Eso hace bastante fácil irte la documentación, encontrar el API relacionado con lo que quieras hacer y ver cómo se ha de hacer, aunque no tengas ni idea del API de WordPress.


### Programar con shortcodes

Un shortcode es un texto envuelto entre corchetes que se puede escribir en un post o página de WordPress y que hace algo, cualquier cosa que te puedas imaginar.

Otro ejemplo que ya he mencionado arriba son las tarjetas de posts y páginas que permite crear enlaces internos a otros contenidos de una manera mucho más visual y atractiva que con un enlace de texto convencional.

Los shortcodes los puedes programar tú mismo a tu medida (como es mi caso con los cuadros de suscripción, por ejemplo), pero también muchísimos plugins con recopilaciones de shortcodes que hacen todo tipo de cosas.

Por ejemplo: son muy típicos las recopilaciones de shortcodes para elementos visuales como botones “bonitos”, recuadros para resaltar textos, tablas de precios, etc.

Piensa ahora en lo siguiente: imagínate que hubiese pegado el HTML para el cuadro de suscripción “a pelo” en cada post.

¿Qué pasaría si lo actualizase?

Pues tendría que actualizar los más o menos 300 posts que tiene esto blog. O sea, antes de empezar ya me habría pegado un tiro…

¿Qué pasa al haber usado un shortcode?

Que actualizo un momento la rutina que es ejecutada por el shortcode y listo. El texto del shortcode como tal sigue siendo el mismo, por tanto, no tengo que tocar ningún post. Sin embargo, todos ellos se habrán actualizado automáticamente.

Tan sencillo y genial, ¿no te parece?

### Programar con hooks

Como has visto, los shortcodes son un mecanismo muy bueno para insertar determinados componentes HTML en tus contenidos y reutilizarlos de una manera inteligente, aunque también se pueden hacer cosas con ellos que no sean visuales.

Hay un mecanismo muy parecido a los shortcodes para hacer eso en posiciones fijas de las páginas, por ejemplo, al principio o al final de un post. Ese mecanismo son los hooks. Hay hooks de WordPress y hooks adicionales que puede añadir, por ejemplo, un determinado tema de WordPress.

Y gracias a otras funciones del API no me tuve que preocupar de nada para disponer de la foto, el nombre y el texto de presentación del autor. Todo eso ya está en la ficha del autor de WordPress y a través del API, WordPress me lo proporciona.

Mi código se lo pide todo al API y el API ya se ocupa de darme la información correcto según el post del que se trata y de lo único de lo que se ocupa mi código es de maquetar esa información.


**Plugins**
Los plugins son un mecanismo magnífico para ampliar la funcionalidad de WordPress.

Ya habrás instalado muchos en tu sitio WordPress y no creo que te tenga que explicar a estas alturas mucho más sobre lo que es un plugin.

También habrás visto el potencial que tienen, especialmente si has usado plugins tan potentes como, por ejemplo, WooCommerce que convierte WordPress en una tienda online completa. Así que imagínate el potencial que tienes este mecanismo para desarrollar tus propias cosas.

Simplemente decirte que es muy fácil empezar a hacer plugins sencillos, aunque hacer plugins avanzados sí que te va a requerir un conocimiento más profundo del API de plugins de WordPress.

En mi caso estoy usando en este momento un total de ocho plugins propios. Uno de ellos es el plugin para las cajitas de suscripción a la lista de correo y el correspondiente popup.

Otro ejemplo es la cajita para buscar dentro del blog que puedes ver en la página home y después de las cajitas de autor en todas las páginas y otro el que añade el botón de Twitter en los comentarios.

Y dentro de poco verás en funcionamiento un plugin de desarrollo propio muy “gordo” con el que estrenaremos una parte completamente nueva y muy importante dentro de Ciudadano 2.0 y que será algo bastante especial que no existe como tal que yo sepa en otros sitios de nuestro nicho 

**Temas hijos**
El último ejemplo de casos típicos de programación WordPress es el caso de los temas hijo que ya mencioné anteriormente.

Es un mecanismo muy elegante de WordPress para poder crear temas WordPress personalizados que se basen en otro tema (el tema padre) y permitan modificarlo a discreción sin el riesgo de verse afectados por una actualización del tema padre como ocurría en nuestro ejemplo de tocar directamente el código de un tema WordPress sin respetar la buena práctica de crear un tema hijo para ello.

Crear la plantilla básica para un tema hijo WordPress es increíblemente fácil. Es una hoja de estilo con nombre style.css con un comentario con esta estructura de campos en las primeras líneas:
