﻿
**TEMAS FILLOS**

Un tema fillo en WordPress é un tema que herda a funcionalidade de outro tema, chamado tema pai. Os temas fillos permiten modificar ou engadir as funcionalidades do tema pai. Un tema fillo é a forma máis segura e sinxela de modificar un tema existente, tanto se se quere facer pequenos cambios coma outros máis amplos.

As razóns para usar un tema fillo son as seguintes:

-   Se se modifica un tema existente e se actualiza, os cambios se perderán. Cun tema fillo, se pode subir o tema pai e seguir mantendo os cambios.
    
-   Pode acelerar o tempo de desenvolvemento do tema.
    
-   É unha boa forma de empezar se se está empezando a aprender sobre o desenvolvemento de temas en WordPress, como é o noso caso.7
    

**Crear un tema fillo**

Para crear un tema fillo hai que seguir os seguintes pasos:

-   Crear unha carpeta no directorio de temas para albergar o tema fillo. A carpeta polo tanto debe estar aloxada en /dir_instalacion_WordPress/wp-content/themes/. Para nomeala non pode haber ningún espazo como parte do nome e é habitual usar o nome do tema pai seguido de “-fillo”. Por exemplo, se estamos facendo un fillo do tema tema _Twenty Fourteen_, o nome da carpeta debería ser tema t_wentyfourteen-fillo_.
    
-   Crear un arquivo chamado style.css, na carpeta do tema fillo. Este é o único arquivo requirido para facer un tema fillo. A folla de estilos debe empezar coas seguintes liñas:
![enter image description here](imgs/tema_fillo_css.png)

Pódese cambiar cada unha das liñas para personalizar o tema. A única liña que debe ter un contido concreto é a liña que indica cal é o tema pai: “Template”. Neste caso o valor que acompaña a esta etiqueta debe ser o nome do tema pai, que no noso caso de exemplo é twentyfourteen.

-   Importar os estilos do tema pai no tema fillo: os estilos do tema pai non son importados automaticamente como ocorre con outros ficheiros de patrón de PHP do pai, polo que se se desexa utilizalos hai que importalos á folla de estilos do tema fillo. Para realizar esta importación, no exemplo móstrase a seguinte liña, que o que fai é importar a folla de estilos do tema pai:
    

@import url("../twentyfourteen/style.css");

Actualmente este non é unha boa práctica senón que é engadir unha acción wp_enqueue_scripts e usar wp_enqueue_style() no arquivo functions.php do tema fillo. Ver o seguinte epígrafe para ver como engadir este código.

Calquera cambio adicional que se queira realizar nalgún estilo do pai, simplemente hai que copiar o estilo desde o tema pai, pegalo no arquivo style.css do fillo e realizar os cambios desexados sobre o estilo.

-   Crear o arquivo functions.php: para realizar a importación do arquivo style.css é necesario incluír un pequeno código no arquivo functions.php do tema fillo, o que nos obriga a crealo.
    

A estrutura de functions.php é sinxela: unha etiqueta PHP de inicio e unha de peche e entre elas o código que se queira escribir. O código, como mínimo deberá conter as sentenzas mínimas para a importación dos estilos como se ve no seguinte exemplo:
![enter image description here](imgs/tema_funcions.png)

O exemplo anterior só funcionará se o tema pai só usa o arquivo principal style.css para manter todos os estilos. En caso de que use máis arquivos .css, entón haberá que asegurarse de que se manteñen todas as dependencias co tema pai.

Agora o tema fillo xa está listo para a activación. Se vamos ao panel de administración e a opción dos temas, pódese ver no listado de temas o tema fillo listo para poder activalo.

-   Realizar a Tarefa 2 “Creación dun novo tema”, onde o alumnado deberá crear un novo tema facendo uso da estratexia de temas fillos.
    

**Arquivos de patrón**

Se se quere cambiar algo máis que a folla de estilos, o tema fillo pode sobreescribir calquera arquivo do tema pai, simplemente hai que incluír un arquivo do mesmo nome na carpeta do tema fillo e sobreescribirase o arquivo equivalente na carpeta do tema pai.

Por exemplo, se queremos cambiar o código PHP para a cabeceira, podemos incluír un arquivo header.php no directorio do tema fillo, e este arquivo será usado en lugar do arquivo header.php do tema pai.

Tamén se poden engadir arquivos no tema fillo que non estean incluídos no tema pai.

Usando o arquivo functions.php

A diferenza do arquivo style.css, o functions.php dun tema fillo non sobreescribe ao tema pai. En lugar diso, cárgase engadíndoo ao arquivo functions.php do tema pai. Concretamente, cárgase xusto antes do tema pai.

Poderíase pensar que unha forma rápida de engadir máis funcionalidade é engadir a función necesaria no arquivo functions.php. Isto ten o problema de que cando se actualice o tema a función desaparecerá. A alternativa é engadir o código nun arquivo functions.php dentro do tema fillo. A función fará o mesmo traballo pero coa vantaxe que non se verá afectada por futuras actualizacións do tema pai.

No seguinte exemplo móstrase o arquivo elemental de functions.,php mostrado anteriormente, ao que simplemente se lle engade a funcionalidade de engadir un enlace de favicon ao elemento head de páxinas HTML.
![enter image description here](imgs/tema_funcions2.png)

Engadindo arquivos no tema fillo

Cando se necesite engadir arquivos que residen dentro da estrutura de directorios do tema fillo utilizarase:

get_stylesheet_directory ()

Con esta función o que se fai é apuntar ao directorio do tema fillo, e non ao tema pai.

Así, o seguinte código mostra como se pode utilizar esta función para facer referencia a un arquivo almacenado dentro da estrutura de directorio do tema fillo:

    require_once( get_stylesheet_directory() . '/o_meu_arquivo.php' );

