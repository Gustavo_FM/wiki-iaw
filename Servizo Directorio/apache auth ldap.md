﻿
##  Autenticación contra un directorio LDAP desde Apache

O servidor Web Apache non usa PAM como sistema de autenticación, senón que emprega unha serie de directivas propias, 
As restricións de acceso realízanse, normalmente, sobre o contido de directorios mediante a directiva Directory

Para que o Apache use o OpenLDAP como sistema de autenticación primeiro hai que habilitar o módulo authnz_ldap:

    sudo a2enmod authnz_ldap

A continuación hai que configurar as directivas dentro de Directory
Imos necesitar sempre, un usuario co que facer as procuras na árbore de LDAP:

    # Parámetros básicos de configuración para a autenticación contra LDAP
    AuthBasicProvider ldap
    AuthType Basic
    
    # Se falla a autenticación LDAP o usuario no se poderá autenticar con outros métodos.
    AuthLDAPBindAuthoritative off
    AuthName "Acceso restrinxido"
    
    # Fase I: AUTENTICACIÓN contra LDAP
    #Este usuario é o que vai a buscar aos demáis usuarios na árbore LDAP.
    AuthLDAPBindDN "CN=consultas,CN=Users,DC=proba,DC=lan"
    AuthLDAPBindPassword abc123..
    #Indicamos o enderezo IP ou nome do servidor de LDAP/Active Directory, e a OU na que imos a buscar aos usuarios.
    #AuthLDAPURL "ldap://192.168.10.30/OU=usuarios,DC=proba,DC=lan?sAMAccountName?sub?(objectClass=*)"
	AuthLDAPURL "ldap://192.168.10.30/OU=usuarios,DC=proba,DC=lan?uid"
    
    # Fase II: AUTORIZACIÓN contra LDAP
    AuthLDAPGroupAttributeIsDN on
    # Autorización a calquera usuario válido
    require valid-user
    
    #Autorización aos membros do grupo "Grupo1"
    #require ldap-group CN=Grupo1,OU=Grupos,DC=midominio,DC=local

